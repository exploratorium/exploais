var fs = require('fs');
var payloadDict = {};
var payloadChars = "0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVW`abcdefghijklmnopqrstuvw";
var aisformat = JSON.parse(fs.readFileSync(__dirname + '/aisformat.json', 'utf8'));
//var sixbitAscii = '@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_' + '  "#$%&' + "'" + "()*+,-./0123456789:;<=>?";
var sixbitAscii = "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_ !\"#$%&`()*+,-./0123456789:;<=>?";
var sixbitDict = {};
for (i = 0; i < sixbitAscii.length; i++) {
  sixbitDict[numToSixBitString(i)] = sixbitAscii[i];
}

for (i = 0; i < payloadChars.length; i++) {
  payloadDict[payloadChars[i]] = numToSixBitString(i);
}

class Decoder {
  constructor() {
    this._queue = {};
  }

  decode(encoded) {
    var i, val;
    var parts = encoded.split(',');
  
    if (parts[0] != "!AIVDM") {
      //console.error("ERR: not an !AIVDM message");
    }
  
    var numSegments = parts[1];
    var segmentIndex = parts[2];
    var id = parts[3];
    var payload = parts[5];
    var checksumList = encoded.substring(1).split('*');
  
    if (!testChecksum(checksumList[0], parseInt(checksumList[1], 16))) {
      //console.error("ERR: checksum error");
    }
  
    if (!payload) {
      return;
    }
  
    if (numSegments > 1) {
      if (!this._queue[id]) {
        this._queue[id] = [];
      }
      
      this._queue[id][segmentIndex - 1] = payload;
      for (i = 0; i < numSegments; i++) {
        if (!this._queue[id][i]) {
          return;
        }
      }
      payload = this._queue[id].join("");
      delete this._queue[id];
    }
  
    var bits = "";
    for (i = 0; i < payload.length; i++) {
      bits += payloadDict[payload[i]];
      var bits = "";
      for (i = 0; i < payload.length; i++) {
        bits += payloadDict[payload[i]];
      }
      var type = parseInt(bits.substring(0, 6), 2);
      if ((type < 1) || (type > aisformat.length)) {
        return;
      }
      var output = {};
  
      if ((type < 1) || (type > aisformat.length)) {
        console.error("ERR: type out of range");
        return;
      }
  
      for (i = 0; i < aisformat[type - 1].fields.length; i++) {
        var field = aisformat[type - 1].fields[i];
        var ss = bits.substring(field.start, field.start + field.length);
        let val = this.parseField(field, ss);
        if (field.callback) {
          val = global[field.callback](val);
        }
        output[field.name] = val;
      }
      return output;
    };
  };
  
}

class TextDecoder extends Decoder {
  constructor() {
    super();
  }

  parseField(field, ss) {
    switch (field.type[0]) {
      case 'u': 
        return parseInt(ss, 2);
        break;
      case 'U': 
        return parseInt(ss, 2) / Math.pow(10, parseInt(field.type[1]));
        break;
      case 'i':
        return parseSigned(ss);
        break;
      case 'I':
        return parseSigned(ss) / Math.pow(10, parseInt(field.type[1]));
        break;
      case 'b':
        return (ss == '1');
        break;
      case 'e':
        return field.enum[parseInt(ss, 2)];
        break;
      case 't':
        return bitsToString(ss);
        break;
      case 'd':
        return ss;
        break;
    }
  }
}

class ValDecoder extends Decoder {
  constructor() {
    super();
  }

  parseField(field, ss) {
    switch (field.type[0]) {
      case 'u': 
        return parseInt(ss, 2);
        break;
      case 'U': 
        return parseInt(ss, 2) / Math.pow(10, parseInt(field.type[1]));
        break;
      case 'i':
        return parseSigned(ss);
        break;
      case 'I':
        return parseSigned(ss) / Math.pow(10, parseInt(field.type[1]));
        break;
      case 'b':
        return (ss == '1');
        break;
      case 'e':
        return parseInt(ss, 2);
        break;
      case 't':
        return bitsToString(ss);
        break;
      case 'd':
        return ss;
        break;
    }
  }
}

function parseSigned(x) {
  if (x[0] == '0') {
    return parseInt(x, 2);
  }
  return parseInt(x, 2) - Math.pow(2, x.length);
};

function testChecksum(s, cs) {
  var checksum = 0;
  for(var i = 0; i < s.length; i++) { 
    checksum = checksum ^ s.charCodeAt(i);
  }
  return (cs == checksum);
}

function bitsToString(bits) {
    var i = 0;
    var s = "";
    var c;
    while (i < bits.length - 1) {
      c = sixbitDict[bits.substring(i, i + 6)];
      s += (c == '@' ? '' : c);
      i += 6;
    }
    return s;
  };

function numToSixBitString(i) {
  var s = i.toString(2);
  while (s.length < 6) {
    s = "0" + s;
  }
  return s;
};

global.rot = function(x) {
  if (x == 0) {
    return "not turning";
  }
  if (x == 128) {
    return "no information available";
  }
  return 4.733 * Math.sqrt(Math.abs(x))* (x < 0 ? -1 : 1);
};

global.degrees = function(x) {
  return x / 60.0;
};

module.exports = {
  createTextDecoder: function() {
    return new TextDecoder();
  },
  createValDecoder: function() {
    return new ValDecoder();
  }
};
