# ExploAIS.js#

ExploAIS is a pure Javascript library for decoding [Automatic Identification System (AIS)](https://en.wikipedia.org/wiki/Automatic_identification_system) data.

### Usage ###
Check out the [example](https://bitbucket.org/exploratorium/exploais/src/b485854d1ac58edf52059b98f6f9e8aced4b6d7c/examples/example.js?at=master). It's pretty straight forward.

### Contact ###
For questions, please contact Eyal Shahar, eshahar@exploratorium.edu

### Further reading ###
* ESR explains AIS in [great detail](https://gpsd.gitlab.io/gpsd/AIVDM.html). This taught me everything I know.
* I sometimes post about my work on AIS feeds in my [blog](http://quickndirty.no-ip.net/index.php/category/shipmapping/).


### License
Unless stated otherwise, all content is licensed under the Creative Commons Attribution License and code licensed under the MIT License. Copyright (c) 2016 Eyal Shahar, Exploratorium.